# alerts/models.py
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.conf import settings
from stores.models import Product
from bottles.models import Sweetness

import logging

from .services import NotificationServiceProvider


def calculate_average_price_per_liter():
    # Calculate the average price per liter of all products (bottles)
    avg_price_per_liter = Product.objects.annotate(
        price_per_liter=models.F('price') / (models.F('bottle__volume_in_milliliters') / 1000)
    ).aggregate(models.Avg('price_per_liter'))['price_per_liter__avg']

    return avg_price_per_liter or 0


class Alert(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    message_text = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    sent_at = models.DateTimeField(default=None, null=True)

    def trigger(self):
        alert_preferences = AlertPreferences.objects.get(user=self.user)
        notification_method = alert_preferences.notification_method
        try:
            notification_method.send(self.message_text)
            self.sent_at = timezone.now()
            self.save()
        except Exception as e:
            # Write details to log
            logging.error(e)
            # Display Error Message to User
            raise Exception("Could not send notification, \
                             please try again later or \
                             use a different notification method.")


class AlertPreference(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    zip_codes = ArrayField(
        models.CharField(max_length=5), 
        default=list
    )
    chains = models.ManyToManyField('stores.Chain')
    sweetness = ArrayField(
        models.CharField(max_length=32,
                         choices=Sweetness.choices,
                         default=Sweetness.DRY.name)
    )
    brands = models.ManyToManyField('bottles.Brand')

    minimum_price_per_liter = models.DecimalField(max_digits=5, decimal_places=2)
    alert_frequency = models.DurationField()
    alert_time = models.TimeField()
    notification_method_class = models.CharField(max_length=32, choices=NotificationServiceProvider.choices, default=NotificationServiceProvider.EMAIL.name)

    @property
    def notification_method(self):
        class_name = self.notification_method_class
        return NotificationServiceProvider.get(class_name)

    def set_minimum_price_per_liter_to_current_average(self):
        self.minimum_price_per_liter = calculate_average_price_per_liter()


