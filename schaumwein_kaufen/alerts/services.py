from abc import ABC
from django.db import models


class NotificationService(ABC):

    def send(self, alert):
        pass


class EmailNotificationService(NotificationService):
    def send(self, alert):
        pass


class SignalNotificationService(NotificationService):
    def send(self, alert):
        pass


class TelegramNotificationService(NotificationService):
    def send(self, alert):
        pass


class NotificationServiceProvider(models.TextChoices):
    EMAIL = 'EmailNotificationService', 'Email'
    SIGNAL = 'SignalNotificationService', 'Signal'
    TELEGRAM = 'TelegramNotificationService', 'Telegram'

    _instances = {}

    @classmethod
    def get(cls, service_name):
        if service_name not in cls._instances:
            if service_name == cls.EMAIL:
                cls._instances[service_name] = EmailNotificationService()
            elif service_name == cls.SIGNAL:
                cls._instances[service_name] = SignalNotificationService()
            elif service_name == cls.TELEGRAM:
                cls._instances[service_name] = TelegramNotificationService()
        return cls._instances.get(service_name)
