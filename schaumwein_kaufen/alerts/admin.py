from django.contrib import admin

from .models import Alert, AlertPreference

# Register your models here.
admin.site.register(Alert)
admin.site.register(AlertPreference)
