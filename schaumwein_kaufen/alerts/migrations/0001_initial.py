# Generated by Django 4.2.1 on 2023-05-28 12:23

from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('bottles', '0001_initial'),
        ('stores', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AlertPreference',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('zip_codes', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=5), default=list, size=None)),
                ('sweetness', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(choices=[('Dry', 'Dry'), ('Semi Dry', 'Semi Dry'), ('Semi Sweet', 'Semi Sweet'), ('Sweet', 'Sweet')], default='DRY', max_length=32), size=None)),
                ('minimum_price_per_liter', models.DecimalField(decimal_places=2, max_digits=5)),
                ('alert_frequency', models.DurationField()),
                ('alert_time', models.TimeField()),
                ('notification_method_class', models.CharField(choices=[('EmailNotificationService', 'Email'), ('SignalNotificationService', 'Signal'), ('TelegramNotificationService', 'Telegram'), ('{}', ' Instances')], default='EMAIL', max_length=32)),
                ('brands', models.ManyToManyField(to='bottles.brand')),
                ('chains', models.ManyToManyField(to='stores.chain')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Alert',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message_text', models.CharField(max_length=300)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('sent_at', models.DateTimeField(default=None, null=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stores.product')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
