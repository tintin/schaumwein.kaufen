from celery import shared_task

from django.db.models import Q
from products.models import Product

from .models import AlertPreferences, Alert


@shared_task
def check_for_alerts():
    """
    Naive implementation of checking for alerts. 
    This can probably be optimized by using a database trigger or something similar.
    """
    alert_preferences = AlertPreferences.objects.prefetch_related('zip_codes', 'chains', 'brands', 'sweetness').all()

    for preference in alert_preferences:
        zip_codes = preference.zip_codes.values_list('id', flat=True)
        chains = preference.chains.values_list('id', flat=True)
        brands = preference.brands.values_list('id', flat=True)
        sweetness = preference.sweetness.values_list('id', flat=True)

        matching_products = Product.objects.filter(
            Q(store__chain__in=chains) &
            Q(store__zip_code__in=zip_codes) &
            Q(bottle__brand__in=brands) &
            Q(bottle__sweetness__in=sweetness) &
            Q(price_per_liter__lt=preference.minimum_price_per_liter)
        ).select_related('bottle', 'store')

        for product in matching_products:
            alert = Alert(
                user=preference.user,
                product=product,
                message_text=f"The product {product.bottle} from {product.store} is now available for {product.price} per liter."
            )
            alert.save()
            alert.trigger()
