from django.contrib.postgres.fields import JSONField
from django.db import models

from simple_history.models import HistoricalRecords


class Chain(models.Model):
    name = models.CharField(max_length=100, primary_key=True)

    def __str__(self):
        return self.name


class Store(models.Model):
    chain = models.ForeignKey('stores.Chain', on_delete=models.CASCADE)
    address_json = models.JSONField()
    postal_code = models.CharField(max_length=10)

    @property
    def address(self):
        """
        Format the address from the json field
        """
        return f'{self.address_json["street"]} {self.address_json["city"]}'

    def __str__(self):
        return f'{self.chain} {self.address}'


class Product(models.Model):
    """
    A product is a bottle in a specific store. 
    The price hisotry is tracked using the SimpleHistory package.
    """
    bottle = models.ForeignKey('bottles.Bottle', on_delete=models.CASCADE)
    store = models.ForeignKey('stores.Store', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    price_history = HistoricalRecords()

    @property
    def price_per_liter(self):
        return self.price / self.bottle.volume_in_mililiters * 1000

    def __str__(self):
        return f'{self.bottle} @ {self.store}'
