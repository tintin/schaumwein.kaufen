from django.contrib import admin

from .models import Chain, Store, Product

admin.site.register(Chain)
admin.site.register(Store)
admin.site.register(Product)
