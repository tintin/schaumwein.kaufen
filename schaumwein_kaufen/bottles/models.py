from enum import Enum

from django.db import models


class Sweetness(models.TextChoices):
    DRY = 'Dry'
    SEMI_DRY = 'Semi Dry'
    SEMI_SWEET = 'Semi Sweet'
    SWEET = 'Sweet'


class Brand(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Bottle(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    sweetness_category = models.CharField(max_length=20,
                                          choices=Sweetness.choices)
    volume_in_milliliters = models.IntegerField()
    alcohol_by_volume_percentage = models.DecimalField(max_digits=3, decimal_places=1)

    def __str__(self):
        return f'{self.brand} {self.sweetness} {self.volume_in_milliliters}ml {self.alcohol_by_volume_percentage}%'

    # Shorthands
    @property
    def sweetness(self):
        return self.sweetness_category

    @property
    def volume(self):
        return self.volume_in_milliliters

    @property
    def alcohol(self):
        return self.alcohol_by_volume_percentage
