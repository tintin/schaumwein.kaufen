from django.db import models

import requests

from .scrapers import ScraperProvider


class StoreScraper(models.Model):
    """
    A StoreScraper is a model that is responsible for scraping a store's
    products at a given interval.
    It provides a common interface for scraping products from different stores
    by using different scrapers.
    """
    store = models.OneToOneField('stores.Store', on_delete=models.CASCADE)
    tracked_products = models.ManyToManyField('stores.Product')
    last_scrape = models.DateTimeField(null=True, blank=True)
    scraper_class = models.CharField(max_length=100, choices=ScraperProvider.choices)

    def track(self, product):
        self.tracked_products.add(product)

    def update_product_data(self, product):
        self.scraper.get_price_data(self.store, product)

    @property
    def scraper(self):
        """
        Returns the instance of the scraper class specified
        """
        return Scrapers.get(self.scraper_class)



