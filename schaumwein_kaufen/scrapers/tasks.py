from celery import shared_task

@shared_task
def update_product_data_task(product_id):
    product = Product.objects.get(id=product_id)
    scrape_manager = ScrapeManager.objects.first()
    scrape_manager.update_product_data(product)

@shared_task
def update_store_data_task(store_id):
    store = Store.objects.get(id=store_id)
    scrape_manager = ScrapeManager.objects.first()
    scrape_manager.update_store_data(store)

