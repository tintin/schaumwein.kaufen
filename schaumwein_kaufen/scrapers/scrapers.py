from abc import ABC, abstractmethod
from django.db import models
import datetime

class BaseScraper(ABC):
    base_url: str
    interval = datetime.timedelta(hours=24)

    @abstractmethod
    def get_store_list(self, zip_code: str):
        ...

    @abstractmethod
    def get_product_list(self, store_id: str):
        ...

    @abstractmethod
    def get_product_data(self, store_id: str, product_id: str):
        ...

    @abstractmethod
    def get_store_data(self, store_id: str):
        ...


class AldiNordWebsiteScraper(BaseScraper):
    base_url = 'https://www.aldi-nord.de/'

    def get_store_list(self, zip_code: str):
        ...

    def get_product_list(self, store_id: str):
        ...

    def get_product_data(self, store_id: str, product_id: str):
        ...

    def get_store_data(self, store_id: str):
        ...


class ReweMobileScraper(BaseScraper):
    base_url = 'https://www.rewe-mobile.de/'

    def get_store_list(self, zip_code: str):
        ...

    def get_product_list(self, store_id: str):
        ...

    def get_product_data(self, store_id: str, product_id: str):
        ...

    def get_store_data(self, store_id: str):
        ...


class PennyMobileScraper(BaseScraper):
    base_url = 'https://www.penny-mobile.de/'

    def get_store_list(self, zip_code: str):
        ...

    def get_product_list(self, store_id: str):
        ...

    def get_product_data(self, store_id: str, product_id: str):
        ...

    def get_store_data(self, store_id: str):
        ...


class LidlWebsiteScraper(BaseScraper):
    base_url = 'https://www.lidl.de/'

    def get_store_list(self, zip_code: str):
        ...

    def get_product_list(self, store_id: str):
        ...

    def get_product_data(self, store_id: str, product_id: str):
        ...

    def get_store_data(self, store_id: str):
        ...


class ScraperProvider(models.TextChoices):
    ALDI_NORD_WEBSITE = 'AldiNordWebsiteScraper', 'Aldi Nord Website'
    REWE_MOBILE = 'ReweMobileScraper', 'Rewe Mobile'
    PENNY_MOBILE = 'PennyMobileScraper', 'Penny Mobile'
    LIDL_WEBSITE = 'LidlWebsiteScraper', 'Lidl Website'

    _instances = {}

    @classmethod
    def get(cls, scraper_name):
        if scraper_name not in cls._instances:
            if scraper_name == cls.ALDI_NORD_WEBSITE:
                cls._instances[scraper_name] = AldiNordWebsiteScraper()
            elif scraper_name == cls.REWE_MOBILE:
                cls._instances[scraper_name] = ReweMobileScraper()
            elif scraper_name == cls.PENNY_MOBILE:
                cls._instances[scraper_name] = PennyMobileScraper()
            elif scraper_name == cls.LIDL_WEBSITE:
                cls._instances[scraper_name] = LidlWebsiteScraper()
        return cls._instances.get(scraper_name)
