from celery import Celery
from celery.schedules import crontab
from django.conf import settings

app = Celery('myproject')

app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
     'check_for_alerts_every_hour': {
        'task': 'alerts.tasks.check_for_alerts',
        'schedule': 3600,  # execute every hour
    },
}
