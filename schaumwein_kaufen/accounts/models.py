from django.contrib.auth.models import AbstractUser
from django.db import models

from .managers import CustomUserManager


class User(AbstractUser):
    username = None # remove username
    first_name = None  # remove first name
    last_name = None  # remove last name
    email = models.EmailField(unique=True)
    phone_number = models.CharField(max_length=10,
                                    unique=True,
                                    blank=True,
                                    null=True)
    name = models.CharField(max_length=64, blank=True, null=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
