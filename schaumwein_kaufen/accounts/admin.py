from django.contrib import admin
from .models import User

class UserAdmin(admin.ModelAdmin):
    exclude = ('password',)
    ordering = ('email',)
    list_display = ('email', 'name', 'is_superuser')
    search_fields = ('email', 'name')
    list_filter = ('is_superuser',)
    readonly_fields = ('email',)


admin.site.register(User, UserAdmin)
